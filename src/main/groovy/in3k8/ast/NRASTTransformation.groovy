package in3k8.ast

import org.codehaus.groovy.ast.ASTNode
import org.codehaus.groovy.ast.ClassNode
import org.codehaus.groovy.ast.MethodNode
import org.codehaus.groovy.ast.expr.ArgumentListExpression
import org.codehaus.groovy.ast.expr.ConstantExpression
import org.codehaus.groovy.ast.expr.MethodCallExpression
import org.codehaus.groovy.ast.expr.VariableExpression
import org.codehaus.groovy.ast.stmt.ExpressionStatement
import org.codehaus.groovy.ast.stmt.Statement
import org.codehaus.groovy.control.CompilePhase
import org.codehaus.groovy.control.SourceUnit
import org.codehaus.groovy.transform.ASTTransformation
import org.codehaus.groovy.transform.GroovyASTTransformation
import org.codehaus.groovy.ast.stmt.BlockStatement

@GroovyASTTransformation(phase = CompilePhase.SEMANTIC_ANALYSIS)
class NRASTTransformation implements ASTTransformation {

    void visit(ASTNode[] nodes, SourceUnit sourceUnit) {

    List methods = sourceUnit.AST.methods

    methods.findAll {MethodNode method ->
        method.getAnnotations(new ClassNode(WithNR))
    }.each { MethodNode method ->
        Statement startMessage = createPrintlnAst("Starting $method.name")

        List<Statement> existingStatements = ((BlockStatement)method.code).statements
        existingStatements.each {
            println it.statementLabel
        }
        existingStatements.add(0, startMessage)

    }
}

private Statement createPrintlnAst(String message) {
    return new ExpressionStatement(
            new MethodCallExpression(
                    new VariableExpression("this"),
                    new ConstantExpression("println"),
                    new ArgumentListExpression(
                            new ConstantExpression(message)
                    )
            )
    )
}

}
